# DevOps Assessment

## 1. Docker-ayes

For this one you can use make:

To build:
```
make build
```

To run docker scan:
```
make scan
```
I've done the image scan using the Snyk that it's integrated with docker (docker scan)

To run the image:
```
make run
```

Inside the make you can change the username, so you can deploy to your dockerhub user.

But if you prefer running the command by yourself:
```
DHUB_USER="leonardocuquejo" # or your dockerhub user
docker build -t ${DHUB_USER}/litecoin:0.18.1 -f litecoin/Dockerfile litecoin

docker scan --severity medium ${DHUB_USER}/litecoin:0.18.1

docker run --rm ${DHUB_USER}/litecoin:0.18.1
```


## 2. k8s FTW:

First, you need to publish the previous image to someplace. I'm pushing to dockerhub.
```
make publish
```

Then deploy to your k8s cluster:
```
kubectl apply -f k8s/litecoin.yaml -n some_name_space
```
Obs: If you have pushed this image to your dockerhub username you need to update the line 39 of the file:
[k8s/litecoin.yaml](k8s/litecoin.yaml)

## 3. All the continuouses:
For this one I took a bit more of time, because I've built a new GKE cluster to be able to test the pipeline in production-like environment, then had to integrate with gitlab-runners to be able to deploy to GKE.

The integration was done using the file: [ci/setup_gcloud.sh](ci/setup_gcloud.sh)

The variables GCLOUD_SERVICE_KEY, GOOGLE_PROJECT_ID that are sensitive data, I've added to GitLab ci variables (Masked and Protected), but in a real-world I should use hashicorp vault or at least the secrets from k8s integrated with the GitLab runners.

I've used the Gitlab security scanner, has you can see on [.gitlab-ci.yml](.gitlab-ci.yml) file.

[Gitlab pipeline](https://gitlab.com/lcuquejo/devops-challenge/-/pipelines)

Because I can't give access to my GKE I'm sharing some screenshots of the pod / k8s.

![Google GKE](screenshots/gke.png)

![K8s output](screenshots/k8s.png)
