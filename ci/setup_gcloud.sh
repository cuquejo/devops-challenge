#!/bin/bash
# I already have this script from a previous job :)
# Basically what it does is install the google-cloud sdk,
# kubectl and setup de project based on GITLAB projected variables

if [ -f "${HOME}/gcloud-service-key.json" ]; then
    echo "Setting up gcloud:"
    apt-get install -y apt-transport-https ca-certificates gnupg
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
    apt-get update && apt-get install google-cloud-sdk kubectl
    gcloud auth activate-service-account --key-file=${HOME}/gcloud-service-key.json
    gcloud --quiet config set project $GOOGLE_PROJECT_ID
    gcloud --quiet config set compute/zone $GOOGLE_COMPUTE_ZONE
    gcloud --quiet config set compute/region $GOOGLE_COMPUTE_REGION
    gcloud container clusters get-credentials --region $GOOGLE_COMPUTE_REGION $GKE_CLUSTER
else
    echo "Missing GCLOUD_SERVICE_KEY"
    exit 1
fi
